﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


public class LaserPointer : MonoBehaviour {

	public SteamVR_Input_Sources handType;
	public SteamVR_Behaviour_Pose controllerPose;
	public SteamVR_Action_Boolean teleportAction;

	public GameObject laserPrefab; // 1 This is a reference to the Laser prefab.
	private GameObject laser; // 2 laser stores a reference to an instance of the laser.
	private Transform laserTransform; // 3 The transform component is stored for ease of use.
	private Vector3 hitPoint; // 4 This is the position where the laser hits.

	
	public Transform cameraRigTransform; //The transform of [CameraRig].
								
	public GameObject teleportReticlePrefab; //Stores a reference to the teleport reticle prefab.

	private GameObject reticle; //A reference to an instance of the reticle.

	private Transform teleportReticleTransform; //Stores a reference to the teleport reticle transform for ease of use.

	public Transform headTransform; //Stores a reference to the player’s head (the camera).

	public Vector3 teleportReticleOffset; //The reticle offset from the floor, so there’s no “Z-fighting” with other surfaces.

	public LayerMask teleportMask; //A layer mask to filter the areas on which teleports are allowed.

	private bool shouldTeleport; //Set to true when a valid teleport location is found.



	// Use this for initialization
	void Start () {
		//Spawn a new laser and save a reference to it in laser.
		laser = Instantiate(laserPrefab);
		//Store the laser’s transform component.
		laserTransform = laser.transform;

		//Spawn a new reticle and save a reference to it in reticle.
		reticle = Instantiate(teleportReticlePrefab);
		//Store the reticle’s transform component.
		teleportReticleTransform = reticle.transform;

	}

	// Update is called once per frame
	void Update () {
		//If the Teleport action is activated:
		if (teleportAction.GetState(handType))
		{
			RaycastHit hit;

			//Shoot a ray from the controller. If it hits something, make it store the point where it hit and show the laser.
			//The mask makes sure the laser can only hit GameObjects that you can teleport to.
			if (Physics.Raycast(controllerPose.transform.position, transform.forward, out hit, 100, teleportMask))

			{
				hitPoint = hit.point;
				ShowLaser(hit);
				//Show the teleport reticle.
				reticle.SetActive(true);
				//Move the reticle to where the raycast hit with the addition of an offset to avoid Z-fighting.
				teleportReticleTransform.position = hitPoint + teleportReticleOffset;
				//Set shouldTeleport to true to indicate the script found a valid position for teleporting.
				shouldTeleport = true;

			}
		}
		else //Hide the laser when the Teleport action desactivates and hides the reticle in the absence of a valid target.
		{
			laser.SetActive(false);
			reticle.SetActive(false);

		}

		if (teleportAction.GetStateUp(handType) && shouldTeleport)
		{
			Teleport();
		}


	}

	private void ShowLaser(RaycastHit hit)
	{
		//Show the laser.
		laser.SetActive(true);
		//Position the laser between the controller and the point where the raycast hits. You use Lerp because you can give it two positions and the percent it should travel. If you pass it 0.5f, which is 50%, it returns the precise middle point.
		laserTransform.position = Vector3.Lerp(controllerPose.transform.position, hitPoint, .5f);
		//Point the laser at the position where the raycast hit.
		laserTransform.LookAt(hitPoint);
		//Scale the laser so it fits perfectly between the two positions.
		laserTransform.localScale = new Vector3(laserTransform.localScale.x,
												laserTransform.localScale.y,
												hit.distance);
	}

	private void Teleport()
	{
		//Set shouldTeleport to false when teleportation is in progress.
		shouldTeleport = false;
		//Hide the reticle.
		reticle.SetActive(false);
		//Calculate the difference between the positions of the camera rig’s center and the player’s head.
		Vector3 difference = cameraRigTransform.position - headTransform.position;
		//Reset the y-position for the above difference to 0, because the calculation doesn’t consider the vertical position of the player’s head.
		difference.y = 0;
		//Move the camera rig to the position of the hit point and add the calculated difference. Without the difference, the player would teleport to an incorrect location.
		cameraRigTransform.position = hitPoint + difference;
	}


}
